﻿/***********************************************************************
 * Nom & Prénom : Sedric Ouambo Silatchom
 * Numéro       : 2712447
 * Date         : 15/01/2023
 * Exercice     : JeuTicTacToe
 * ********************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Tic_Tac_Toe
{
    internal class JeuTicTacToe
    {

        public const int tailleGrille = 4;

        /* Possibilite de changer la taille de la grille en 4, 5, ...
         * Si l'on modifie la constante tailleGrille pour la faire passer à 4, 5 ..., par exemple, 
         * la grille sera dynamiquement modifiée et toutes les fonctions seront toujours opérationnelles.
         * Il est donc possible dans une autre version, de donner à l'utilisateur, 
         * la possibilite de choisir la taille de la grille, ou alors de faire 
         * passer l'utilisateur automatiquement à une autre etape de jeu, avec une taille plus grande */

        public static String nomJoueur1 = "";
        public static String nomJoueur2 = "";
        public static String nomJoueurCourant = "";         // C'est le joueur qui a la main pour jouer

        public static char symboleJoueur1 = 'X';
        public static char symboleJoueur2 = 'O';

        public const int decalageCentre = 20;
        public const int decalageDroite = 2;
        public const int decalageGauche = 1;
        public const int decalageGauche2 = 2;

        //Ce bloc defini les couleurs que nous avons utilises dans le code
        public static ConsoleColor rouge = ConsoleColor.Red;
        public static ConsoleColor vert = ConsoleColor.White;
        public static ConsoleColor bleu = ConsoleColor.Blue;
        public static ConsoleColor jaune = ConsoleColor.DarkMagenta;
        public static ConsoleColor CouleurCourant;

        public static int caseChoisi;


        public static String[] grille;                  // Tableau unidimentionnel de chaine de caracteres

        public static void tab(int decalage)
        {
            /*
            * Argument : un arguement qui est un entier
            * Type de retour : void
            * Description : Permet de faire plusieurs tabulations correspondants a l'entier pris en argument
            *               Cela nous permet d'afficher notre grille à l'endroit souhaité.
            */
            Console.Write(new String('\t', decalage));
        }

        public static void SaisirNom()
        {
            /*
             * Argument : aucun
             * Type de retour : void
             * Description : Lire et sauvegarder les noms des deux joueurs dans des variables correspondantes
             */
            Console.Title = "Jeu TIC TAC TOE By Ouambo  @2023";

            Console.WriteLine();
            tab(decalageGauche);
            Console.WriteLine("Bienvenue dans le jeu TIC TAC TOE");

            Console.WriteLine();
            tab(decalageGauche);
            Console.Write("Saisir le nom du Joueur 1 : ");
            nomJoueur1 = Console.ReadLine();

            Console.WriteLine();
            tab(decalageGauche);
            Console.Write("Saisir le nom du Joueur 2 : ");
            nomJoueur2 = Console.ReadLine();
        }

        public static void InitialiserGrille()
        {
            /*
            * Argument : aucun
            * Type de retour : void
            * Description : Initialiser la grille avec du vide
            */
            grille = new string[tailleGrille];
            for (var i = 0; i < grille.Length; i++)
                grille[i] = new string(' ', tailleGrille);  //On rempli chaque entree du tableau par une chaine contenant du vide

            nomJoueurCourant = nomJoueur1;                  // Le premier joueur a avoir la main est le joueur1       
        }

        public static void AfficherGrille()
        {
            /*
            * Argument : aucun
            * Type de retour : void
            * Description : Afficher la grille de jeu
            */

            Console.Clear();

            tab(decalageCentre);
            Console.WriteLine("Bienvenue dans le Jeu TIC TAC TOE \n");

            tab(decalageGauche);
            Console.WriteLine("Le but est de remplir la grille de gauche avec pour reférence,");
            tab(decalageGauche);
            Console.WriteLine("les numéros se trouvant dans la grille de droite \n");

            tab(decalageGauche);
            Console.Write("Joueur 1 : ");
            Console.BackgroundColor = bleu;
            Console.Write(nomJoueur1);
            Console.ResetColor();
            Console.Write(" Symbole de jeu = ");
            Console.BackgroundColor = bleu;
            Console.Write(symboleJoueur1);
            Console.ResetColor();
            Console.WriteLine();

            int ind = 1;

            foreach (var chaine in grille)
            {
                tab(decalageGauche2);
                for (int j = 0; j < tailleGrille; j++)
                    Console.Write("    -   ");

                tab(decalageDroite);
                for (int j = 0; j < tailleGrille; j++)
                    Console.Write("    -   ");

                Console.WriteLine();


                tab(decalageGauche2);

                Console.Write("| ");

                if (chaine[0] == ' ')
                    CouleurCourant = vert;
                else
                    if (chaine[0] == 'X')
                    CouleurCourant = bleu;
                else
                    CouleurCourant = jaune;

                Console.BackgroundColor = CouleurCourant;
                Console.Write("  " + chaine[0] + "  ");
                Console.ResetColor();

                Console.Write(" |");

                for (int i = 1; i < tailleGrille; i++)
                {
                    Console.Write(" ");

                    if (chaine[i] == ' ')
                        CouleurCourant = vert;
                    else
                        if (chaine[i] == 'X')
                        CouleurCourant = bleu;
                    else
                        CouleurCourant = jaune;

                    Console.BackgroundColor = CouleurCourant;
                    Console.Write("  " + chaine[i] + "  ");
                    Console.ResetColor();

                    Console.Write(" |");
                }


                tab(decalageDroite);
                Console.Write("|   {0}   |", ind);
                for (int i = 1; i < tailleGrille; i++)
                {
                    ind++;
                    Console.Write("   {0}   |", ind);
                }

                ind++;
                Console.WriteLine();
            }

            tab(decalageGauche2);
            for (int j = 0; j < tailleGrille; j++)
                Console.Write("    -   ");

            tab(decalageDroite);
            for (int j = 0; j < tailleGrille; j++)
                Console.Write("    -   ");

            Console.WriteLine("\n");
            tab(decalageGauche);

            Console.Write("Joueur 2 : ");
            Console.BackgroundColor = jaune;
            Console.Write(nomJoueur2);
            Console.ResetColor();
            Console.Write(" Symbole de jeu = ");
            Console.BackgroundColor = jaune;
            Console.Write(symboleJoueur2);
            Console.ResetColor();

            tab(decalageDroite);
            Console.BackgroundColor = vert;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write("Case vide  ");

            Console.BackgroundColor = bleu;
            Console.Write("   ");
            Console.ResetColor();

            Console.Write("Case de ");
            Console.ForegroundColor = bleu;
            Console.Write(nomJoueur1 + " ");
            Console.ResetColor();

            Console.BackgroundColor = jaune;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write("Case de ");
            Console.ForegroundColor = jaune;
            Console.Write(nomJoueur2);
            Console.ResetColor();

            Console.WriteLine();

        }

        public static int SaisirNombre()
        {
            /*
            * Argument : aucun
            * Type de retour : un entier qui correspond a la case choisie
            * Description : Demander au joeur qui a la main de choisir un indice correspondant à la case qu'il souhaite remplir.
            */

            int choix = 0;

            Console.WriteLine();
            while (true)
            {


                bool testFormat = false;
                while (!testFormat)
                {
                    tab(decalageGauche);
                    if (nomJoueurCourant.Equals(nomJoueur1))
                        Console.ForegroundColor = bleu;
                    else
                        Console.ForegroundColor = jaune;

                    Console.Write(nomJoueurCourant);
                    Console.ResetColor();

                    try
                    {
                        Console.Write(" à toi de jouer. Quelle case vide veux-tu remplir ? [1 - {0}] : ", tailleGrille * tailleGrille);
                        choix = Int32.Parse(Console.ReadLine());
                        testFormat = true;
                    }
                    catch
                    {
                        testFormat = false;
                        tab(decalageGauche);
                        Console.ForegroundColor = rouge;
                        Console.WriteLine("Format de donnee incorrect, Entrer un entier");
                        Console.ResetColor();
                        Console.WriteLine();
                    }
                }

                if (choix < 1 || choix > tailleGrille * tailleGrille)
                {
                    Console.ForegroundColor = rouge;
                    tab(decalageGauche);
                    Console.WriteLine("Indice incorrect ! L'indice doit appartenir à [1 - {0}]", tailleGrille * tailleGrille);
                    Console.ResetColor();
                    Console.WriteLine();
                }
                else
                    break;

            }

            return choix;
        }

        public static int GenererCordonnees(int choixJoueur, out int colonne)
        {
            /*
           * Argument : un entier correspondant a la case choisi par le joeur ayant la main
           * Type de retour : un entier correspondant a la ligne de la case choisi
           *                  un parametre de type out est aussi utlise pour retourne la colonne de la case chois
           *                  On peut donc dire que cette fonction retourne 2 entiers (lignet et colonne)
           * Description : Prend en entree la case choisi par le joueur et retourne sa ligne et sa colonne dans la grille
           */
            int i = 0, j = 0, compteur = 0;

            while (i < tailleGrille)
            {
                j = 0;

                while (j < tailleGrille)
                {
                    if (compteur == choixJoueur)
                        break;
                    else
                    {
                        j++; compteur++;
                    }

                }

                if (compteur == choixJoueur)
                    break;
                else
                    i++;

            }

            colonne = j - 1;        // A la sortie de la boucle, i = ligne et j = colonne de la case choisi
            return i;
        }

        public static void RemplirCase()
        {
            /*
           * Argument : aucun
           * Type de retour : void
           * Description : Demande au joueur de chosir une case, verifie que la case est vide
           *                Sinon, il y a repetition de la demande. Et enfin, marque la case choisie.
           *                Cette fonction fait appel aux fonctions, SaisirNombre et GenererCordonness
           */

            int lig, col;

            while (true)
            {
                // La boucle permet de demander a l'utilisateur de choisir une case vide,
                // tant que la case qu'il a choisi n'est pas vide.

                lig = GenererCordonnees(SaisirNombre(), out col);

                if (grille[lig].ElementAt(col) == ' ')              // On verifie si la case choisie est vide
                {
                    String ch = grille[lig].Remove(col, 1);
                    Console.WriteLine();

                    if (nomJoueurCourant.Equals(nomJoueur1))
                        grille[lig] = ch.Insert(col, symboleJoueur1 + "");
                    else
                        grille[lig] = ch.Insert(col, symboleJoueur2 + "");

                    break;                                         // Si la case choisie est vide, on ajoute le symbole 
                                                                   // du joueur dans la grille et on arrete la boucle
                }
                else
                {
                    Console.ForegroundColor = rouge;
                    tab(decalageGauche);
                    Console.WriteLine("Case deja remplie, Bien vouloir choisir une case vide");
                    Console.ResetColor();
                    Console.WriteLine();
                }
            }
        }

        public static bool ChaineGagnante(String chaine)
        {
            /*
             * Argument : aucun
             * Type de retour : bool
             * Description : Verifie si une chaine contient (ligne ou colonne ou diagonale ou antidiagonale)
             *               contient uniquement les symboles d'un joueur quelconque,
             *               Dans ce cas, cette chaine est une chaine qui entraine une victoire
             */
            if ((chaine.Equals(new String(symboleJoueur1, tailleGrille)) ||
                    chaine.Equals(new String(symboleJoueur2, tailleGrille))) &&
                    !chaine.Equals(new String(' ', tailleGrille)))

                return true;

            return false;
        }

        public static bool LigneGagnante()
        {
            /*
             * Argument : aucun
             * Type de retour : bool
             * Description : Parcours les progressivement les lignes et si tous les elements d'une 
             *               ligne sont identiques, alors on retourne true cas il y a un vainqueur.
             *               Retourne false si le parcours entier n'est pas fructueux.
             *               Cette fonction fait appel a la fonction ChaineGagnante
             */

            foreach (var chaine in grille)
            {
                if (ChaineGagnante(chaine))
                    return true;
            }
            return false;
        }

        public static bool ColonneGagnante()
        {
            /*
            * Argument : aucun
            * Type de retour : bool
            * Description : Parcours les progressivement les colonnes et si tous les elements d'une 
            *               colonne sont identiques, alors on retourne true cas il y a un vainqueur.
            *               Retourne false si le parcours entier n'est pas fructueux.
            *               Cette fonction fait appel a la fonction ChaineGagnante
            */
            for (int i = 0; i < tailleGrille; i++)
            {
                char[] table = new char[tailleGrille];

                for (int j = 0; j < tailleGrille; j++)
                    table[j] = grille[j].ElementAt(i);

                String chaine = new string(table);
                if (ChaineGagnante(chaine))
                    return true;
            }
            return false;
        }

        public static bool AntiDiagonaleGagnante()
        {
            /*
            * Argument : aucun
            * Type de retour : bool
            * Description : Parcours l'antidiagonale et verifie si tous ses elements sont identiques.
            *               Dans ce cas, on retourne true cas il y a un vainqueur
            *               Sinon, on retourne false
            *               Cette fonction fait appel a la fonction ChaineGagnante
            */
            char[] table = new char[tailleGrille];

            for (int i = 0; i < tailleGrille; i++)
                table[i] = grille[i].ElementAt(i);

            String chaine = new string(table);
            if (ChaineGagnante(chaine))
                return true;

            return false;
        }

        public static bool DiagonaleGagnante()
        {
            /*
            * Argument : aucun
            * Type de retour : bool
            * Description : Parcours la diagonale et verifie si tous ses elements sont identiques
            *               Dans ce cas, on retourne true cas il y a un vainqueur
            *               Sinon, on retourne false
            *               Cette fonction fait appel a la fonction ChaineGagnante
            */
            char[] table = new char[tailleGrille];

            int j = tailleGrille - 1;

            for (int i = 0; i < tailleGrille; i++)
            {
                table[i] = grille[i].ElementAt(j);
                j--;
            }
            String chaine = new string(table);
            if (ChaineGagnante(chaine))
                return true;

            return false;
        }

        public static bool Gagner()
        {
            /*
            * Argument : aucun
            * Type de retour : bool
            * Description : Retourne true s'il y a un vainqueur et false dans le cas contraire.
            *               Appel de 4 fonctions :
            *               --> LigneGagante
            *               --> ColonneGagnante
            *               --> DiagonaleGagnante
            *               --> AntiDiagonaleGagnante
            */

            // il y a un vainqueur si au moins l'une des fonctions appelees retourne true

            if (LigneGagnante() || ColonneGagnante() || DiagonaleGagnante() || AntiDiagonaleGagnante())
                return true;

            return false;           //Si on ne retourne pas true, forcement c'est false qui sera retourne
        }

        public static bool GrillePleine()
        {
            /*
            * Argument : aucun
            * Type de retour : bool
            * Description : Retourne true si la grille est pleine et false dans le cas contraire
            */
            foreach (var chaine in grille)              //Parcours des chaines de la grille
            {
                foreach (var caractere in chaine)        // Parcours des caracteres de chaque chaine obtenue
                {
                    if (caractere == ' ')               // Si on trouve un vide, on retourne false et fin de la fonction
                        return false;
                }
            }
            return true;       // Si on sort de la boucle, cela signifie que la grille est pleien, donc retourner true
        }


        public static bool JeuNull()
        {
            /*
            * Argument : aucun
            * Type de retour : bool
            * Description : Retourne true s'il y a match nul et false dans la cas contraire.
            *               Un macth est nul lorsque la grille est pleine et il n'y a pas de vainqueur.
            *               Cette fonction fait appel aux fonctions GrillePleine et Gagner.
            */

            if (GrillePleine() && !Gagner())
                return true;
            return false;
        }

        public static void Jouer()
        {
            /*
            * Argument : aucun
            * Type de retour : void
            * Description : Executer une partie de jeu
            */

            SaisirNom();
            InitialiserGrille();

            do
            {
                AfficherGrille();
                RemplirCase();
                if (nomJoueurCourant.Equals(nomJoueur1))
                    nomJoueurCourant = nomJoueur2;
                else
                    nomJoueurCourant = nomJoueur1;

            } while (!Gagner() && !JeuNull());        //Fin de la partie

            //Verification s'il y a un vainqueur ou s'il y a match null

            if (Gagner())
            {
                AfficherGrille();

                if (nomJoueurCourant.Equals(nomJoueur1))
                {
                    Console.WriteLine("\n");
                    tab(decalageGauche);
                    Console.ForegroundColor = jaune;
                    Console.WriteLine("Felicitations {0} !!! Tu es le  vainqueur.", nomJoueur2);
                    Console.WriteLine();
                    Console.ResetColor();

                }
                else
                {
                    Console.WriteLine("\n");
                    Console.ForegroundColor = bleu;
                    tab(decalageGauche);
                    Console.WriteLine("Felicitations {0} !!! Tu es le  vainqueur.", nomJoueur1);
                    Console.WriteLine();
                    Console.ResetColor();
                }
            }
            else
            {
                if (JeuNull())
                {
                    AfficherGrille();

                    Console.WriteLine("\n");
                    //Console.BackgroundColor = vert;
                    Console.ForegroundColor = vert;
                    tab(decalageGauche);
                    Console.WriteLine("Vous avez fait match Null !!!");
                    Console.WriteLine();
                    Console.ResetColor();
                }
            }
        }

        public static void Main(String[] args)
        {
            string jeu;

            while (true)
            {
                // Joeur tant que l'utilisateur dit Oui

                Console.Clear();

                Jouer();

                Console.WriteLine();
                tab(decalageGauche);
                Console.Write("Voulez - vous rejouer ? (O/N) : ");

                jeu = Console.ReadLine().ToUpper();

                if (!jeu.Equals("O"))
                {
                    Console.WriteLine();
                    tab(decalageGauche);

                    Console.Write("Aurevoir et à la prochaine !!!  Appuyer sur la touche ENTREE pour quitter....");
                    break;
                }

            }
            Console.ReadLine();
        }
    }
}
